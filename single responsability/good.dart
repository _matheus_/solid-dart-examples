import 'shapes.dart';

class AreaSumCalculator {
  List<Shape> shapes;

  AreaSumCalculator(this.shapes);

  num sum() {
    final areas = shapes.map((s) => s.calcArea());

    return areas.reduce((a, b) => a + b);
  }
}

class SumCalcOutputter {
  AreaSumCalculator areaCalculator;

  SumCalcOutputter(this.areaCalculator);

  String asString() {
    return areaCalculator.sum().toString();
  }

  String asJson() {
    return '{"result": ${areaCalculator.sum()}}';
  }

  String asXml() {
    return '<result>${areaCalculator.sum()}</result>';
  }
}

void main() {
  final shapes = <Shape>[Circle(1), Circle(.5), Square(4), Square(6)];
  final calculator = AreaSumCalculator(shapes);
  final outputter = SumCalcOutputter(calculator);

  print(outputter.asString());
  print(outputter.asXml());
  print(outputter.asJson());
}
