import 'dart:math';

abstract class Shape {
  num calcArea();
}

class Circle implements Shape {
  num radius;
  Circle(this.radius);

  @override
  num calcArea() {
    return pi * pow(radius, 2);
  }
}

class Square implements Shape {
  num length;
  Square(this.length);

  @override
  num calcArea() {
    return pow(length, 2);
  }
}
