import 'shapes.dart';

class AreaSumCalculator {
  List<Shape> shapes;

  AreaSumCalculator(this.shapes);

  num sum() {
    final areas = shapes.map((s) => s.calcArea());

    return areas.reduce((a, b) => a + b);
  }

  String outputAsPlainString() {
    return sum().toString();
  }

  String outputAsJson() {
    return '{"result": ${sum()}}';
  }

  String outputAsXml() {
    return '<result>${sum()}</result>';
  }
}

void main() {
  final shapes = <Shape>[Circle(1), Circle(.5), Square(4), Square(6)];
  final calculator = AreaSumCalculator(shapes);

  print(calculator.outputAsJson());
  print(calculator.outputAsXml());
  print(calculator.outputAsPlainString());
}
