abstract class AnimalInterface {
  void eat();
  void sleep();
}

abstract class FlyInterface {
  void fly();
}

class Bird implements AnimalInterface, FlyInterface {
  @override
  void eat() {
    // lógica para comer
  }

  @override
  void fly() {
    // lógica para voar
  }

  @override
  void sleep() {
    // lógica para dormir
  }
}

class Dog implements AnimalInterface {
  @override
  void eat() {
    // lógica para comer
  }

  @override
  void sleep() {
    // lógica para dormir
  }
}

void main() {
  final papagaio = Bird();
  papagaio.fly();
  papagaio.eat();
  papagaio.sleep();

  final labrador = Dog();
  labrador.sleep();
  labrador.eat();
}
