class Employee {
  String name;
  Manager? manager;
  double salary = 0.0;

  Employee(this.name);

  void assingManager(Manager manager) {
    // Lógica para atribuição de gerente
    this.manager = manager;
  }

  void calcSalary(int rank) {
    salary = (12.0 + (rank * 2)) * 220;
  }
}

class Manager extends Employee {
  Manager(String name) : super(name);

  @override
  void calcSalary(int rank) {
    salary = (16.0 + (rank * 3)) * 220;
  }
}

class CEO extends Employee {
  CEO(String name) : super(name);

  @override
  void calcSalary(int rank) {
    salary = (26.0 + (rank * 4)) * 220;
  }

  @override
  void assingManager(Manager manager) {
    throw new Exception('CEO não possui gerente');
  }
}

void main() {
  var manager = Manager('Alberto');
  manager.calcSalary(2);

  var employee = CEO('Lucas');
  employee.calcSalary(1);
  //employee.assingManager(manager);

  printPayRoll(employee);
}

void printPayRoll(Employee employee) {
  print('O salário do ${employee.name} é de R\$ ${employee.salary}');
}
