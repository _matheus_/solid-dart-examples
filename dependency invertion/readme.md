# Dependency Injection

### Problema
Existe duas classes responsáveis por armazenar instâncias da classe `Shape`, uma armazena os dados na memória (`ShapesRepositoryInMemory`) e a outra em arquivo (`ShapeRepositoryFile`). A classe `ShapeService` é que está sendo responsável por instânciar a implementação desejada.


### Correto
Foi criada a interface `ShapeRepositoryInterface`, além das outras classes. Agora a classe `ShapeService` recebe em seu construtor uma intância do tipo `ShapeRepositoryInterface`
