# Single Responsability

### Problema

A classe `AreaSumCalculator` possui as funcionalidades de realizar a soma das áreas das formas geométricas informadas, além de ter métodos que apresentam esse resultado em diferentes formatos, como *JSON*, *XML* e *String*.

### Correto

A lógica é separada em duas classes. A classe `AreaSumCalculator`, que é a responsável por fazer o somatório das áreas e a  classe `SumCalcOutputter` que possui os métodos que irão apresentar o resultado nos diferentes formatos.