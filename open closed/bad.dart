class Circle {
  num radius;

  Circle(this.radius);
}

class Square {
  num length;

  Square(this.length);
}

class ShapeDrawer {
  void drawShapes(List<dynamic> shapes) {
    for (final shape in shapes) {
      if (shape is Square) {
        print('Desenhando um quadrado de tamanho ${shape.length}');
      } else if (shape is Circle) {
        print('Dessenhando um circule de raio ${shape.radius}');
      }
    }
  }
}

void main() {
  final shapes = [Circle(1.3), Circle(.6), Square(2), Square(4)];

  final drawer = ShapeDrawer();

  drawer.drawShapes(shapes);
}
