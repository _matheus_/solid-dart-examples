# Interface Segragation

### Problema
Existe a interface `AnimalInterface` com os métodos `eat`, `sleep` e `fly`. 
E existe duas classes que implementam esta interface que são: `Bird` e `Dog`. Mas o classe `Dog`  não precisa do método `fly`

### Correto

Foi criada a  interface `AnimalInterface` com os métodos `eat` e`sleep` e a interface `FlyInterface`, somente com o método `fly`. Agora a classe `Bird` implementa ambas interfaces e a classe `Dog` implementa somente a classe `AnimalInterface`