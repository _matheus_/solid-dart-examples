abstract class Shape {
  String draw();
}

class Circle implements Shape {
  num radius;

  Circle(this.radius);

  @override
  String draw() {
    return 'Dessenhando um circule de raio $radius';
  }
}

class Square implements Shape {
  num length;

  Square(this.length);

  String draw() {
    return 'Desenhando um quadrado de tamanho $length';
  }
}

class ShapeDrawer {
  void drawShapes(List<Shape> shapes) {
    for (final shape in shapes) {
      print(shape.draw());
    }
  }
}

void main() {
  final shapes = [Circle(1.3), Circle(.6), Square(2), Square(4)];

  final drawer = ShapeDrawer();

  drawer.drawShapes(shapes);
}
