abstract class EmployeeInterface {
  void calcSalary(int rank);
}

abstract class ManagedInterface {
  void assingManager(Manager manager);
}

abstract class BaseEmployee implements EmployeeInterface {
  String name;
  double salary = 0.0;
  BaseEmployee(this.name);

  @override
  void calcSalary(int rank) {
    salary = (12.0 + (rank * 2)) * 220;
  }
}

class Employee extends BaseEmployee implements ManagedInterface {
  Manager? manager;

  Employee(String name) : super(name);

  void assingManager(Manager manager) {
    // Lógica para atribuição de gerente
    this.manager = manager;
  }
}

class Manager extends Employee {
  Manager(String name) : super(name);

  @override
  void calcSalary(int rank) {
    salary = (16.0 + (rank * 3)) * 220;
  }
}

class CEO extends BaseEmployee {
  CEO(String name) : super(name);

  @override
  void calcSalary(int rank) {
    salary = (26.0 + (rank * 4)) * 220;
  }
}

void main() {
  var manager = Manager('Alberto');
  manager.calcSalary(2);

  var employee = CEO('Lucas');
  employee.calcSalary(1);
  //employee.assingManager(manager);

  printPayRoll(employee);
}

void printPayRoll(BaseEmployee employee) {
  print('O salário de ${employee.name} é de R\$ ${employee.salary}');
}
