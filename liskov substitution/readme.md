# Liskov Substitution

### Problema
Existem 3 classes que representam diferentes cargos em uma empresa. `Employee`, `Manager` e `CEO`. 
A classe `Employee` é a classe mãe das demais, que possui o método de calcular salário (`calcSalary`) e atribuir um gerente (`assingManager`). 
Acontece que a classe filha `CEO` não tem o comportamento de atribuir um gerente (já que o CEO é o cargo máximo) . Por isso esta classe lança uma excessão para evitar que este método seja chamado.


### Correto
Foram criadas interfaces que descrevem os comportamentos que os empregados devem possuir. A interface `EmployeeInterface` possui o método de calcular salário, e a interface `ManagedInterface` possui o método de atribuição de gerente.
Foi criada a classe abstrata `BaseEmployee` com as propriedades em comum a todas as outras e que implementa a interface `EmployeeInterface` que por sua vez é extendida por todas as demais classes: `Employee`, `Manager` e `CEO`. 

Somente as classes `Employee` e `Manager` implementam a interface `ManagedInterface`. Com isso a classe `CEO`  não possui mais o método `assingManager`