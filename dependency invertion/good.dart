class Shape {}

abstract class ShapeRepositoryInterface {
  List<Shape> getAllShapes();

  void addShape(Shape shape);
}

class ShapeRepositoryFile implements ShapeRepositoryInterface {
  final String _path;

  ShapeRepositoryFile(this._path);

  List<Shape> getAllShapes() {
    // ler registros do arquivo
    return [];
  }

  void addShape(Shape shape) {
    // add registro no arquivo
  }
}

class ShapesRepositoryInMemory implements ShapeRepositoryInterface {
  final _inMemoryShapes = <Shape>[];

  List<Shape> getAllShapes() {
    return _inMemoryShapes;
  }

  void addShape(Shape shape) {
    _inMemoryShapes.add(shape);
  }
}

class ShapeService {
  final ShapeRepositoryInterface _repository;

  ShapeService(this._repository);

  void createShape(Shape shape) {
    _repository.addShape(shape);
  }
}

void main() {
  final _fileRepository = ShapeRepositoryFile('/base/path/shapes.txt');
  final _inMemoryRepository = ShapesRepositoryInMemory();

  final shapeService = ShapeService(_fileRepository);
  // final shapeService = ShapeService(_inMemoryRepository);

  shapeService.createShape(Shape());
}
