# Open closed

### Problema

A classe `ShapeDrawer` recebe uma lista de formas geométricas e, de acordo com o tipo da forma, faz seu desenho.

### Correto
A lógica de desenho passa para a própria classe da forma geométrica. Para isso, as formas devem implementr a interface `Shape`, para que tenham o  método `draw` e implementem a sua lógica. Agora a classe `ShapeDrawer`  chama o método `draw` das formas geométricas.
