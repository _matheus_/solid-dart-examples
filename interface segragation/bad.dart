abstract class AnimalInterface {
  void eat();
  void fly();
  void sleep();
}

class Bird implements AnimalInterface {
  @override
  void eat() {
    // lógica para comer
  }

  @override
  void fly() {
    // lógica para voar
  }

  @override
  void sleep() {
    // lógica para dormir
  }
}

class Dog implements AnimalInterface {
  @override
  void eat() {
    // lógica para comer
  }

  @override
  void fly() {
    throw new Exception('Cachorro não voa');
  }

  @override
  void sleep() {
    // lógica para dormir
  }
}


void main() {
  final papagaio = Bird();
  papagaio.fly();
  papagaio.eat();
  papagaio.sleep();

  final labrador = Dog();
  labrador.sleep();
  labrador.fly();
}