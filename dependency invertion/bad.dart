class Shape {}

class ShapeRepositoryFile {
  final String _path;

  ShapeRepositoryFile(this._path);

  List<dynamic> getAllShapes() {
    // ler registros do arquivo
    return [];
  }

  void addShape(Shape shape) {
    // add registro no arquivo
  }
}

class ShapesRepositoryInMemory {
  final _inMemoryShapes = <Shape>[];

  List<dynamic> getAllShapes() {
    return _inMemoryShapes;
  }

  void addShape(Shape shape) {
    _inMemoryShapes.add(shape);
  }
}

class ShapeService {
  late ShapesRepositoryInMemory _inMemoryRepository;
  late ShapeRepositoryFile _fileRepository;

  ShapeService() {
    _inMemoryRepository = ShapesRepositoryInMemory();
    _fileRepository = ShapeRepositoryFile('/base/path/shapes.txt');
  }

  void createShape(Shape shape) {
    _inMemoryRepository.addShape(shape);
    _fileRepository.addShape(shape);
  }
}

void main() {
  var shapeService = ShapeService();
  shapeService.createShape(Shape());
}
